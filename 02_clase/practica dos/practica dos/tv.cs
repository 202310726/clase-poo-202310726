﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace practica_dos
{
    class tv
    {
        public int tamanio = 0;
        public int volumen = 0;
        public string color = "";
        public string brillo = "";
        public string canal = "";

        public void settamanio(int tamanio)
        {
            this.tamanio = tamanio;
        }
        public int gettamanio()
        {
            return this.tamanio;
        }
        public void setbrillo(string brillo)
        {
            this.brillo = brillo;
        }
        public string getbrillo()
        {
            return this.brillo;
        }
        public void setcolor(string color)
        {
            this.color = color;
        }
        public string getcolor()
        {
            return this.color;
        }
        public void setcanal(string canal)
        {
            this.canal = canal;
        }
         public string getcanal()
        {
            return this.canal;
        }
        public void setvolumen(int volumen)
        {
            this.volumen = volumen;
        }
        public int getvolumen()
        {
            return this.volumen;
        }


    }
}
