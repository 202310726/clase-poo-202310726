﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Class1
    {
       
        int suma1;
       

        public void entrada1()
        {
            suma1 = 12 + 6;
            Console.WriteLine("suma de 12+6 = {0}  ", suma1);
        }
        private void entrada2()
        {
            suma1 = 20 + 7;
            Console.WriteLine("suma de 20+7 =  {0}  ", suma1);
        }
        protected void entrada3()
        {
            suma1 = 99 + 5;
            Console.WriteLine("suma de 99+5 =  {0} ", suma1);
        }
        public void entrada2pub()
        {
            entrada2();
        }
    }
    class Clase2:Class1
    {
        public void entra1()
        {
            entrada1();
        }
        public void entrada3()
        {
            entrada3();
        }
        public void entrada2()
        {
            entrada2pub();
        }
    }
    public class Constructor
    {
        private int num1, num2;
        public Constructor(int nume = 8, int nume2 = 88)
        {
            num1 = nume;
            num2 = nume2;
            num1 = num1 + num2;
            Console.WriteLine("El resultado de {0} + {1} = {2}", nume, num2, num1);
        }
        Constructor()
        {
            Console.WriteLine("aqui esta el destructor");
        }
    }
}
