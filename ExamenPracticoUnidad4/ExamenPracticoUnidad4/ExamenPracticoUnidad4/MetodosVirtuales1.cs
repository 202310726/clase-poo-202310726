﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenPracticoUnidad4
{
    class MetodosVirtuales1
    {
        public virtual void CalcularOperacion(double n1, double n2)
        {
            double resultado = n1 * n2;
            Console.WriteLine("El resultado de la multiplicacion en la clase metodo virtual es: " + resultado);
        }

    }
}
