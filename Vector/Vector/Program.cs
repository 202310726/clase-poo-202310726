﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vector
{
    class Program
    {
        static int[] Entero = new int[20];

        static void Main(string [] args)
        {
            Console.WriteLine("------------| V E C T O R |------------");
            LeeDatos();
            Console.ReadKey();
        }

        static void LeeDatos()
        {
            Console.WriteLine("INGRESA 20 NUMEROS: ");

            for (int i = 0; i < Entero.Length; i++)
            {
                Entero[i] = int.Parse(Console.ReadLine());
               
            }
            MostrarDatos(Entero);
        }

        static void MostrarDatos(int[]Entero)
        {
            Console.WriteLine("Mostrar Numeros Ingresados");

            for (int i = 0; i < Entero.Length; i++)
            {
                Console.Write(Entero[i]+", ");
            }
        }
    }
}
