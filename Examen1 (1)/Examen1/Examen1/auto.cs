﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Examen1
{
    class auto
    {
        private string modelo = "";
        private string marca = "";
        private int puertas = 0;
        private int velocidad = 0;
        public void setvelocidad (int velocidad)
        {
            this.velocidad = velocidad;
        }
        public int getvelocidad()
        {
            return this.velocidad;
        }
        public void setpuertas(int puertas)
        {
            this.puertas = puertas;
        }
        public int getpuertas()
        {
            return this.puertas;
        }
        public void setmodelo(string modelo)
        {
            this.modelo = modelo;
        }
        public string getmodelo()
        {
            return this.modelo;
        }
        public void setmarca(string marca)
        {
            this.marca = marca;
        }
        public string getmarca()
        {
            return this.marca;
        }
    }
}
